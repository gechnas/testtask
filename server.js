const http = require("http");
const fs = require("fs");
const path = require("path");

const PORT = 3000;
const STATIC_DIR = "static";

const ROUTES = {
  "/": "index.html",
  "/index.html": "index.html",
  "/script.js": "script.js",
  "/data.json": "data.json",
  "/form.html": "form.html",
  "/form.css": "form.css",
  "/form.js": "form.js"
};

const server = http.createServer((request, response) => {
  if (ROUTES.hasOwnProperty(request.url)) {
    let fileName = request.url === "/" ? "index.html" : request.url;
    let fileStream = fs
      .createReadStream(path.join(__dirname, STATIC_DIR, fileName))
      .on("error", err => {
        console.error(`Can't read file: ${err}`);
        response.writeHead(500);
        response.end();
      });
    fileStream.pipe(response);
  } else {
    response.writeHead(404);
    response.end("Not found");
  }
});

server.listen(PORT, err => {
  if (err) {
    console.error(`Can't open port: ${PORT}`, err);
    return;
  }
  console.log(`Server is started on port ${PORT}`);
});
