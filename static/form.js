const FIELDS = [
  {
    title: "Name",
    name: "name",
    tag: "input",
    required: true,
    min: 1,
    max: 100
  },
  {
    title: "Address 1",
    name: "address1",
    tag: "input",
    required: true,
    min: 1,
    max: 100
  },
  {
    title: "Address 2",
    name: "address2",
    tag: "input",
    required: false,
    min: 0,
    max: 100
  },
  {
    title: "City",
    name: "city",
    tag: "input",
    required: true,
    min: 1,
    max: 50
  },
  {
    title: "State",
    name: "state",
    tag: "select",
    options: [
      {
        title: "California",
        name: "CA"
      },
      {
        title: "New York",
        name: "NY"
      }
    ],
    required: true
  },
  {
    title: "Zip Code",
    name: "zipcode",
    tag: "input",
    type: "number",
    required: true,
    min: 5,
    max: 5
  }
];

function renderOption(name, title) {
  let option = document.createElement("option");
  option.value = name;
  option.innerText = title;
  return option;
}

function renderControl(tag, type, name, options = []) {
  let control = document.createElement(tag);
  if (type) {
    control.type = type;
  }
  control.name = name;
  control.addEventListener("change", () => hideError(control));
  if (tag === "select") {
    control.appendChild(renderOption("", "Choose one."));
    options.forEach(({ name, title }) => {
      control.appendChild(renderOption(name, title));
    });
  }
  return control;
}

function renderButtons(buttons) {
  let container = document.createElement("div");
  container.className = "buttons";

  buttons.forEach(({ title, action }) => {
    let btn = document.createElement("button");
    btn.innerText = title;
    btn.addEventListener("click", action);
    container.appendChild(btn);
  });
  return container;
}

function renderForm(fields) {
  let form = document.createElement("form");
  for (let field of fields) {
    let group = document.createElement("div");
    group.className = "group";

    let titleCol = document.createElement("div");
    titleCol.classList = "col title";
    titleCol.innerText = field.title;

    let controlCol = document.createElement("div");
    controlCol.classList = "col control";
    controlCol.appendChild(
      renderControl(field.tag, field.type, field.name, field.options)
    );
    let errorContainer = document.createElement("div");
    errorContainer.className = "error";
    controlCol.appendChild(errorContainer);

    group.appendChild(titleCol);
    group.appendChild(controlCol);

    form.appendChild(group);
  }
  let buttons = renderButtons([
    {
      title: "OK",
      action: e => {
        e.preventDefault();
        validateForm(form, fields);
      }
    },
    {
      title: "Reset",
      action: e => {
        e.preventDefault();
        resetForm(form, fields);
      }
    }
  ]);
  form.appendChild(buttons);
  return form;
}

function hideError(control) {
  let errorContainer = control.nextSibling;
  errorContainer.innerText = "";
}

function displayError(control, errorText = "Error") {
  let errorContainer = control.nextSibling;
  errorContainer.innerText = errorText;
}

function validateForm(form, fields) {
  fields.forEach(field => {
    let control = form[field.name];
    let value = control.value;
    let error = displayError.bind(null, control);
    if (field.required && !value) {
      error("This field is required");
    } else if (typeof field.min !== undefined && value.length < field.min) {
      error(
        `This field should contain at least ${field.min} ${
          field.type === "number" ? "digits" : "chars"
        } long.`
      );
    } else if (typeof field.max !== undefined && value.length > field.max) {
      error(
        `This field shouldn't be loger that ${field.max} ${
          field.type === "number" ? "digits" : "chars"
        }.`
      );
    } else if (field.type === "number" && !value.match(/^\d+$/)) {
      error(`This field should contain digits only.`);
    } else {
      hideError(control);
    }
  });
}

function resetForm(form, fields) {
  fields.forEach(field => {
    let control = form[field.name];
    hideError(control);
    if (field.tag === "select") {
      control.selectedIndex = 0;
    } else {
      control.value = "";
    }
  });
}

let container = document.getElementById("form-container");
container.innerText = "";
let form = renderForm(FIELDS);
container.appendChild(form);
