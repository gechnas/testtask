const API_BASE_URL = "http://localhost:3000";

function fetchTree() {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", `${API_BASE_URL}/data.json`);
    xhr.onload = () => {
      try {
        let json = JSON.parse(xhr.response);
        return resolve(json);
      } catch (e) {
        reject();
      }
    };
    xhr.onerror = reject;
    xhr.send();
  });
}

function findNodeLabel(tree, nodeId) {
  for (let node of tree) {
    if (node.id === nodeId) {
      return node.label;
    }
    if (node.children) {
      let found = findNodeLabel(node.children, nodeId);
      if (found) {
        return found;
      }
    }
  }
  return null;
}

(async () => {
  let tree = await fetchTree();
  let found = findNodeLabel(tree, 5);
  console.log(found ? `Found node with label '${found}'` : "Nothing found");
})();
